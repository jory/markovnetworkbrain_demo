/*
 *  globalConst.h
 *  HMMBrain
 *
 *  Created by Arend on 9/16/10.
 *  Modified by Jory
 *
 */

#pragma once // only include this file once in compilation

// brain states must be a power of 2 due to runtime efficiency tricks
#define nBrainStates 16
const double perSiteMutationRate=0.005;
