/*
 *  Agent.h
 *  HMMBrain
 *
 *  Created by Arend on 9/16/10.
 *  Modified by Jory
 *
 */

#pragma once // only include this file once in compilation

#include "globalConst.h"
#include "HMM.h"
#include <vector>
#include <stdio.h>
#include <stdlib.h>


using namespace std;

static int masterID=0;


class Dot{
public:
	double xPos,yPos;
};


class Agent{
public:
	vector<HMMU*> hmmus;
	vector<unsigned char> genome;
	vector<Dot> dots;					// for output to dot format.
	
	Agent *ancestor;
	unsigned int nrPointingAtMe;
	unsigned char state[nBrainStates], newState[nBrainStates];
	double fitness;
	vector<double> fitnesses;
	
	int ID,nrOfOffspring;
	bool saved;
	bool retired;
	int born;
	
	Agent();
	~Agent();
	void setupRandomAgent(int nucleotides);
	void loadAgent(const char* filename);
	void loadAgentWithTrailer(const char* filename);
	void setupPhenotype(void);										// converts genome gates
	void inherit(Agent *from,double mutationRate,int theTime);
	unsigned char * getStatesPointer(void);
	void updateStates(void);
	void resetBrain(void);
	void seedWithStartCodons(void);
	void showBrain(void);
	void showPhenotype(void);
	void saveToDot(char *filename);
	void saveToDotFullLayout(char *filename);
	
	void initialize(int x, int y, int d);
	Agent* findMRCA(void); // Most Recent Common Ancestor
	void saveLOD(FILE *statsFile, FILE *genomeFile, string experimentID, int replicateID, int progenitorDOB);
	void retire(void);
	void setupDots(int x, int y,double spacing); // Dots, as in graphviz
	void saveLogicTable(FILE *f);
	void saveGenome(FILE *f);

	// custom simulation variables (change these for your experiment)
	int food;
	double xPos,yPos,direction;
	vector <int> input_values;		// used to store input values for invert task 
	int food_collected;

	int red_collected;
	int blue_collected;
	int last_food; // 0 = none; 2 = red; 3 = blue;
	int diet_red; // number of times agent did not eat red
	int diet_blue; // number of times agent did not eat blue.
	int num_changes;

	int red_runs;
	int blue_runs;
	vector <int> red_counts; // counts current run of red (0 if feeding on blue);
	vector <int> blue_counts; // counts current run of red (0 if feeding on blue);

	int left_count;
	int right_count;
	int move_count;
};
