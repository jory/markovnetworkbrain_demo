/*
 *  FitnessEvaluator.h
 *  HMMBrain
 *
 *  Created by Arend on 9/23/10.
 *  Modified by Jory
 *
 */
 
#pragma once // only include this file once in compilation

#include "globalConst.h"
#include "Agent.h"
#include <vector>
#include <map>
#include <set>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

#define xDim 256
#define yDim 16
#define cPI 3.14159265

class FitnessEvaluator{
public:
    int scoreTable[83][9];
	vector<int> evaluate(Agent* agent);
	FitnessEvaluator();
	~FitnessEvaluator();
};
