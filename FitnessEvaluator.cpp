/*
 *  FitnessEvaluator.cpp
 *  HMMBrain
 *
 *  Created by Arend on 9/23/10.
 *  Modified by Jory
 *
 */

#include "FitnessEvaluator.h"
#include <math.h>


FitnessEvaluator::FitnessEvaluator(){
}

FitnessEvaluator::~FitnessEvaluator(){
}

vector<int> FitnessEvaluator::evaluate(Agent* agent){ // pass custom important information here in function params
	vector<int> answers;
	
	double food_fitness_reward = 1;			// fittness gain for eating any food
	double food_change_cost = 1.5;			// fitness cost for switching to other food

	int const size = 10;					// grid size (grid is square)
	int grid[size][size];

	int agentX, agentY, agentD; // location and facing direction of agent
	int Dx[8] = { 0, 1, 1, 1, 0, -1, -1, -1 }; // defines directions
	int Dy[8] = { -1, -1, 0, 1, 1, 1, 0, -1 }; //  "   "   "      "
	char const agent_icon[8] = { '<', '/', 'v', '\\', '>', '/', '^', '\\' }; // used to display agent

	int front_sensor_reading;
	int ground_sensor_reading;

	int red_rate = 50;								// ratio of red berries to blue berries in inital world

	agent->food_collected = 0;
	agent->red_collected = 0;
	agent->blue_collected = 0;
	agent->last_food = 0;							// what sort of food did agent eat last

	agent->left_count = 0;
	agent->right_count = 0;
	agent->move_count = 0;

	int i, j; // used for counters

	for (i = 0; i < size; i++){										// set up world
		for (j = 0; j < size; j++) {
			grid[i][j] = (rand() % 100 < red_rate) ? 2 : 3;			// place a red berry (2) or blue berry (3)
			if ((i == 0) || (j == 0) || (i == size - 1) || (j == size - 1)){
				grid[i][j] = 1;										// Place the walls around the edge
			}
		}
	}

	agentX = (rand() % (size - 2)) + 1;								 // place agent randomly
	agentY = (rand() % (size - 2)) + 1;

	agentD = rand() % 8;											// face agent randomly

	agent->resetBrain();											// reset agent
	agent->fitness = (double)0.0;
	agent->red_collected = 0;
	agent->blue_collected = 0;
	agent->food_collected = 0;
	agent->num_changes = 0;
	agent->last_food = 0;
	agent->diet_red = 0;
	agent->diet_blue = 0;

	agent->red_runs = -1;
	agent->blue_runs = -1;
	agent->red_counts.clear();
	agent->blue_counts.clear();


	for (i = 0; i < 16; i++) agent->state[i] = 0; // clear agent states

	int run_duration = 250;
	for (int t = 0; t < run_duration; t++){
		front_sensor_reading = grid[agentX + Dx[agentD]][agentY + Dy[agentD]];
		agent->state[0] = (front_sensor_reading == 1) ? 1 : 0; // agent is looking at a wall
		agent->state[1] = (front_sensor_reading == 2) ? 1 : 0; // agent is looking at a red berry
		agent->state[2] = (front_sensor_reading == 3) ? 1 : 0; // agent is looking at a blue berry

		ground_sensor_reading = grid[agentX][agentY];
		agent->state[3] = (ground_sensor_reading == 2) ? 1 : 0; // agent is looking at a red berry
		agent->state[4] = (ground_sensor_reading == 3) ? 1 : 0; // agent is looking at a blue berry

		agent->updateStates();											// update brain once (propagate states one gate)

		if ((grid[agentX][agentY] == 2) && (agent->state[13] == 1)) {	// eat red berry
			agent->fitness += food_fitness_reward;
			agent->food_collected++;
			agent->red_collected++;
			grid[agentX][agentY] = 0, 0;
			if (agent->last_food == 3){									// change food type
				agent->fitness -= food_change_cost;
				agent->last_food = 2;
				agent->num_changes++;
				agent->red_runs++;
				agent->red_counts.push_back(0);
			}
			if (agent->last_food == 0) {
				agent->last_food = 2;
				agent->red_runs++;
				agent->red_counts.push_back(0);
			}
			agent->red_counts[agent->red_runs]++;
		}
		if ((grid[agentX][agentY] == 3) && (agent->state[13] == 1)) {	// eat blue berry
			agent->fitness += food_fitness_reward;
			agent->food_collected++;
			agent->blue_collected++;
			grid[agentX][agentY] = 0, 0;
			if (agent->last_food == 2){									// change food type
				agent->fitness -= food_change_cost;
				agent->last_food = 3;
				agent->num_changes++;
				agent->blue_runs++;
				agent->blue_counts.push_back(0);
			}
			if (agent->last_food == 0) {
				agent->last_food = 3;
				agent->blue_runs++;
				agent->blue_counts.push_back(0);
			}
			agent->blue_counts[agent->blue_runs]++;
		}
		if ((grid[agentX][agentY] == 2) && (agent->state[13] != 1)) { agent->diet_red++; }
		if ((grid[agentX][agentY] == 3) && (agent->state[13] != 1)) { agent->diet_blue++; }

		if ((agent->state[14] == 1) && (agent->state[15] == 1)) {				// move forward
			if (grid[agentX + Dx[agentD]][agentY + Dy[agentD]] != 1) {
				if (grid[agentX][agentY] == 0){
					//grid[agentX][agentY] = (agent->last_food == 3) ? 2 : 3;	// place a new berry of the oposite color
					grid[agentX][agentY] = (rand() % 100 < red_rate) ? 2 : 3;	// place a new berry
				}
				agentX += Dx[agentD];											// move agent in direction it is facing
				agentY += Dy[agentD];
				//agent->fitness += .001;										// possible reward for moving?
				agent->move_count++;
			}
		}
		else if (agent->state[14] == 1) {										// turn left
			agentD = (agentD - 1);
			if (agentD == -1) agentD = 7;
			agent->right_count++;
		}

		else if (agent->state[15] == 1) {										// turn right
			agentD = (agentD + 1) % 8;
			agent->left_count++; 
		}
	}
	return answers;
}



