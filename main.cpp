
#include <stdio.h>
#include <stdlib.h>

#include <vector>
#include <map>
#include <math.h>
#include <time.h>
#include <iostream>
#include <iomanip>
#include <thread>
#include <functional> // for ref
#include <string.h>

#include "globalConst.h"
#include "HMM.h"
#include "Agent.h"
#include "FitnessEvaluator.h"
#include "params/params.h" // for argument parsing

#ifdef _WIN32 // for pid() (processid) for srand (for HPCC)
	#include <process.h>
#else
	#include <unistd.h>
#endif

#define randDouble ((double)rand()/(double)RAND_MAX)

using namespace std;
using namespace Params;

int update=0;								// init update

const char* cstr(string s) { return s.c_str(); }

// processes a subsection of the population
void threadedEvaluateFitness(int chunkBegin, int chunkEnd, const vector<Agent*>& agents, FitnessEvaluator& fitnessEvaluator, int& evaluations) {
	for (int chunk_index=chunkBegin; chunk_index<chunkEnd; chunk_index++) {
		for (int replicate_i=0; replicate_i < evaluations; replicate_i++) {
			fitnessEvaluator.evaluate(agents[chunk_index]);
			agents[chunk_index]->fitnesses.push_back(agents[chunk_index]->fitness);
		}
	}
}

/// Expects Parameters: filename_line_of_descent  filename_genome_out
int main(int argc, char *argv[])
{
	// variables
	int i,j,who=0;
	double maxFitness, aveFitness;
	FILE *resFile;
	FILE *LODFile;
	FILE *genomeFile;
	vector<Agent*>agents;
	vector<Agent*>nextGen;
	Agent *rootAgent; // initial agent, special for line of descent traversal
	FitnessEvaluator *fitnessEvaluator;
	vector<thread> threads;
	
	// params to be read from command line
	bool showhelp;
	string filenameLOD;
	string experimentID;
	int replicateID;
	string filenameGenome;
	string filenameStartWith;
	int totalGenerations=0;					// how long will the simulation run?
	int populationSize=0;					// population size
	int evaluations=0;						// how many times to test evaluating fitness of an agent
	//int nthreads=thread::hardware_concurrency(); // get number of processors available automatically
	int nthreads=0;							// how many threads to use

	// argument parsing definitions
	addp(TYPE::BOOL, &showhelp);
	addp(TYPE::STRING, &filenameLOD, "--lod", "filename to save Line of Descent.");
	addp(TYPE::STRING, &experimentID, "--experiment", "unique identifier for this experiment, shared by all replicates.");
	addp(TYPE::INT, &replicateID, "--replicate", "unique number to identify this replicate in this experiment (data output convenience).");
	addp(TYPE::STRING, &filenameGenome, "--genome", "filename to save genomes of the LODFile.");
	addp(TYPE::INT, &totalGenerations, "100", false, "--generations", "number of generations to simulate (updates).");
	addp(TYPE::INT, &populationSize, "100", false, "--populationSize", "size of the population.");
	addp(TYPE::STRING, &filenameStartWith, "none", false, "--startWith", "specify a genome file used to seed the population.");
	addp(TYPE::INT, &evaluations, "3", false, "--evaluations", "number of evaluations for an agent's fitness evaluation.");
	addp(TYPE::INT, &nthreads, "1", false, "--nthreads", (string("number of threads to use. This system reports ")+to_string(thread::hardware_concurrency())+string(" cores available.")).c_str());
	// FORMAT: addp(TYPE, &varableReference, "default_val", isRequired(default True), "--flag", "help description");
	
	argparse(argv);
	if (showhelp) {
		cout << argdetails() << endl;
		cout << "Example minimal invocation:" << endl;
		cout << argv[0] << " --experiment=linearFitness --replicate=1 --lod=lineOfDescent.lod --genome=genome.gen" << endl;
		cout << "or" << endl;
		cout << argv[0] << " --experiment linearFitness --replicate 1 --lod lineOfDescent.lod --genome genome.gen" << endl;
		cout << endl;
		exit(0);
	}

	// initializations
	srand(getpid());
	LODFile=fopen(cstr(filenameLOD),"w+t");
	genomeFile=fopen(cstr(filenameGenome),"w+t");	
	// Read from the command line target LOD out file, and target genome out file
	agents.resize(populationSize);
	fitnessEvaluator=new FitnessEvaluator;
	rootAgent=new Agent;
	//you can also loadAgent to load a non random ancestor to start with
	if (filenameStartWith != "none")
		rootAgent->loadAgent(filenameStartWith.c_str());
	else
		rootAgent->setupRandomAgent(5000);				// set up an agents of length 5000

	rootAgent->setupPhenotype();						// decode the genome
	for(i=0;i<agents.size();i++){						// use rootAgent to make populationSize number of addtional agents
		agents[i]=new Agent;
		agents[i]->inherit(rootAgent,0.01,0);		// would create a population varying around the initial agent
		// using agents[i]->inherit(rootAgent,0.0,0);	// would create a population of clones
	}
	nextGen.resize(agents.size());
	rootAgent->nrPointingAtMe--;
	cout<<"setup complete"<<endl;

	printf("%s	%s	%s\n", "update", "max_w", "avg_w");
	//the main loop
	while(update<totalGenerations){						// run until we run out of time
		for(i=0;i<agents.size();i++){					// set all fitnesses to 0.0
			agents[i]->fitness=0.0;						// clear fitness
			agents[i]->fitnesses.clear();				// clear fitnesses (used to keep track of the fitness of each replicate
		}

		threads.clear();
		int chunksize=agents.size()/nthreads;
		for (int threadid=0; threadid < nthreads; threadid++)
			threads.push_back(thread(threadedEvaluateFitness, chunksize*threadid, chunksize*threadid+chunksize, ref(agents), ref(*fitnessEvaluator), ref(evaluations)));
		if (agents.size()%nthreads != 0) // take care of any uneven division of workload
		{
			threads.push_back(thread(threadedEvaluateFitness, nthreads*chunksize, agents.size(), ref(agents), ref(*fitnessEvaluator), ref(evaluations)));
		}
		for (thread& t : threads) t.join(); // wait for all threads to finish

		maxFitness=0.0;
		aveFitness = 0.0;
		for(i=0;i<agents.size();i++){
														// sum all fintesses in the finessess buffer
			agents[i]->fitness=0.0;
			for(j=0;j<evaluations;j++)
				agents[i]->fitness+=agents[i]->fitnesses[j];
			agents[i]->fitness/=(double)evaluations;		// normalize fitness by replicates

			aveFitness += agents[i]->fitness / agents.size();

			if(agents[i]->fitness>maxFitness){
				who=i;
				maxFitness=agents[i]->fitness;
			}
		}
		printf("%i	%f	%f\n", update, (double)maxFitness, (double)aveFitness);
														// roulette wheel selection
		for(i=0;i<agents.size();i++){
			Agent *d;
			d=new Agent;

			do{
				j=rand()%(int)agents.size();
			} while(randDouble>(agents[j]->fitness/maxFitness));

			d->inherit(agents[j],perSiteMutationRate,update);
			nextGen[i]=d;
		}
														// finished creating the next generation from current, now overwrite current
		for(i=0;i<agents.size();i++){
			agents[i]->retire();
			agents[i]->nrPointingAtMe--;
			if(agents[i]->nrPointingAtMe==0)
				delete agents[i];
			agents[i]=nextGen[i];
		}
		agents=nextGen;
		update++;
	}

	// pick an agent (now at the end of evolution)
	// and save and show information from a couple generations
	// back, which reduces noise from mutations.
	agents[0]->ancestor->ancestor->saveLOD(LODFile,genomeFile, experimentID, replicateID, -1); // -1 tells saveLOD to make header for csv
	agents[0]->ancestor->ancestor->saveGenome(genomeFile); // allows to save a particular agents for reloading

	// uncomment the following 2 lines to see the final brain tables at end of evolution
	//agents[0]->ancestor->ancestor->setupPhenotype();
	//agents[0]->ancestor->ancestor->showPhenotype();

	// save the best agent's genome
	Agent* bestAgent=nullptr;
	maxFitness = 0.0f;
	for (Agent* a : agents) {
		if (a->fitness > maxFitness) {
			maxFitness = a->fitness;
			bestAgent = a;
		}
	}
	if (bestAgent) {
		bestAgent->saveGenome(genomeFile);
	}
	return 0;
}

